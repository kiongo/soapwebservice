
package unprg.service.pe.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "ListaUserResponse", namespace = "http://pe.service.unprg/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaUserResponse", namespace = "http://pe.service.unprg/")
public class ListaUserResponse {

    @XmlElement(name = "return", namespace = "")
    private List<unprg.service.pe.UserPojo> _return;

    /**
     * 
     * @return
     *     returns List<UserPojo>
     */
    public List<unprg.service.pe.UserPojo> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<unprg.service.pe.UserPojo> _return) {
        this._return = _return;
    }

}
