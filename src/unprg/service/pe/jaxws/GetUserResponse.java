
package unprg.service.pe.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getUserResponse", namespace = "http://pe.service.unprg/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserResponse", namespace = "http://pe.service.unprg/")
public class GetUserResponse {

    @XmlElement(name = "return", namespace = "")
    private unprg.service.pe.UserPojo _return;

    /**
     * 
     * @return
     *     returns UserPojo
     */
    public unprg.service.pe.UserPojo getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(unprg.service.pe.UserPojo _return) {
        this._return = _return;
    }

}
