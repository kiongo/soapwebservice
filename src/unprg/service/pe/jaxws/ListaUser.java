
package unprg.service.pe.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "ListaUser", namespace = "http://pe.service.unprg/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaUser", namespace = "http://pe.service.unprg/")
public class ListaUser {


}
