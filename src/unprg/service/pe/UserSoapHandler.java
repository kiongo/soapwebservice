package unprg.service.pe;

import java.util.Iterator;
import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SAAJResult;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.dom.DOMSource;
import unprg.service.pe.jaxws.CreateUser;
import unprg.service.pe.jaxws.DeleteUser;
import unprg.service.pe.jaxws.GetUser;
import unprg.service.pe.jaxws.ListaUser;



public class UserSoapHandler {

	private static final String NAMESPACE_URI="http://pe.service.unprg/";
	private static final QName CREATE_USER_QNAME= new QName(NAMESPACE_URI,"createUser");
	private static final QName GET_USER_QNAME= new QName(NAMESPACE_URI,"getUser");
	private static final QName DELETE_USER_QNAME= new QName(NAMESPACE_URI,"deleteUser");
	private static final QName LISTA_USER_QNAME= new QName(NAMESPACE_URI,"ListaUser");
	private MessageFactory messageFactory;
	private CreateUserAdapter greeterAdapter;
	
	public UserSoapHandler() throws SOAPException{
		messageFactory =MessageFactory.newInstance();
		greeterAdapter =new CreateUserAdapter();
		
	}
	
	@SuppressWarnings("rawtypes")
	public SOAPMessage handleSoapMessage (SOAPMessage request) throws SOAPException {
		
		SOAPBody soapBody=request.getSOAPBody();
		Iterator iterator =soapBody.getChildElements();
		Object responsePojo=null;
		while (iterator.hasNext()) {
			Object next =iterator.next();
			if(next instanceof SOAPElement) {
				SOAPElement soapElement = (SOAPElement) next ;
			
				QName qname =soapElement.getElementQName();
				System.out.print("qname"+ qname);
				
				if(CREATE_USER_QNAME.equals(qname)) {
					
					responsePojo = handleCreateUser(soapElement);
					break;
				}
				
				if(GET_USER_QNAME.equals(qname)) {
					
					responsePojo = handleGetUser(soapElement);
					break;
				}
				if(DELETE_USER_QNAME.equals(qname)) {
					
					responsePojo = handleDeleteUser(soapElement);
					break;
				}
				if(LISTA_USER_QNAME.equals(qname)) {
					
					responsePojo = handleListaUser(soapElement);
					break;
				}
			}
			
			
		}
		SOAPMessage soapResponse =messageFactory.createMessage();
		soapBody =soapResponse.getSOAPBody();
		if(responsePojo !=null){
			JAXB.marshal(responsePojo, new SAAJResult(soapBody));
			
			
		}else {
			SOAPFault fault =soapBody.addFault();
			fault.setFaultString("unrecognize Soap request..");
		}
		
		return soapResponse;
		
	}
	
	private Object handleCreateUser(SOAPElement soapElement){
		CreateUser createuser =JAXB.unmarshal(new DOMSource(soapElement), CreateUser.class);
		return greeterAdapter.createUser(createuser);
	} 
	
	private Object handleGetUser(SOAPElement soapElement){
		GetUser getuser =JAXB.unmarshal(new DOMSource(soapElement), GetUser.class);
		return greeterAdapter.getUser(getuser);
	} 

	private Object handleDeleteUser(SOAPElement soapElement) {
		DeleteUser deleteuser = JAXB.unmarshal(new DOMSource(soapElement), DeleteUser.class);
			
		return greeterAdapter.deleteUser(deleteuser);
	} 
	@SuppressWarnings("unused")
	private Object handleListaUser(SOAPElement soapElement) {
		//ListaUser deleteuser = JAXB.unmarshal(new DOMSource(soapElement), ListaUser.class);
			
		return greeterAdapter.listaUser();
	} 
	
}
