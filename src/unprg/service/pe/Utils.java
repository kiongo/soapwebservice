package unprg.service.pe;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

public class Utils {

	private static DatastoreService ds =DatastoreServiceFactory.getDatastoreService();
	
	public static void updateEntity(Entity e) {
		
		ds.put(e);
	}
	@SuppressWarnings("deprecation")
	public static UserPojo getEntify(String id)  {
		
		
		UserPojo  up =new UserPojo();
		Query query = new Query("Employee");
		query.addFilter("Username", FilterOperator.EQUAL, id);
		PreparedQuery pq = ds.prepare(query);
		try {
		
		Entity e = pq.asSingleEntity();
		up.setUsername(e.getProperty("Username").toString());
		 System.out.println(" Metodo  :  Username "+e.getProperty("Username").toString());
		up.setPassword(e.getProperty("Password").toString());
		 System.out.println(" Metodo  :  Password "+e.getProperty("Password").toString());
		up.setEmail(e.getProperty("Email").toString());
		 System.out.println(" Metodo  :  Email "+e.getProperty("Email").toString());
		up.setAgo(e.getProperty("Ago").toString());
		 System.out.println(" Metodo  :  Ago "+e.getProperty("Ago").toString());
		}catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			
		}
		
		return up;
	}
	
public static void deleteEntity(Key id) {
		
		ds.delete(id);
	}

@SuppressWarnings("null")
public static List<UserPojo> Lista() {
	System.out.println("llego");
	
	Query q = new Query("Employee");

	PreparedQuery pq = ds.prepare(q);

	//List<UserPojo> listar =null;
	List<UserPojo> listar = new ArrayList<UserPojo>();
	
	UserPojo bd=null;
	
	for (Entity result : pq.asIterable()) {
	 String Username = (String) result.getProperty("Username").toString();
	  String password = (String) result.getProperty("Password").toString();
	  String email = (String) result.getProperty("Email").toString();
	  String ago = (String) result.getProperty("Ago").toString();
	  
	  System.out.println("Username : "+Username.toString());
	  System.out.println("password : "+password.toString());
	
		 bd =new UserPojo(Username,password,email,ago);
	  listar.add(bd);
	
	}
	
	return listar;

}


}
