package unprg.service.pe;


import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
@WebService
public class WebServiceDemo {
@WebMethod
	public boolean createUser(String userid,String username,String password , String email,String ago){
		
		Entity employee=new Entity("Employee",userid);
		employee.setProperty("Username",username);
		employee.setProperty("Password",password);
		employee.setProperty("Email",email);
		employee.setProperty("Ago",ago);
		
		System.out.print("username"+username);
		
		Utils.updateEntity(employee);
		
		return  true;
	}
@WebMethod
	public UserPojo getUser(String name) throws EntityNotFoundException{
	System.out.println(" Metodo  :  getUser ");
	System.out.println(" Metodo  :  Username "+name);
	
	//Key key1=KeyFactory.createKey("Employee", name);
	//System.out.println("   key1 : "+name);
	
	UserPojo up=null;
	up = Utils.getEntify(name);
	
	if(up==null) {
		up.setAgo("");
	}
	return up ;
	
	}

	@WebMethod
	public boolean deleteUser(String name) {

		Key key1 = KeyFactory.createKey("Employee", name);

		Utils.deleteEntity(key1);
		return true;

	}
@WebMethod
public List<UserPojo> ListaUser(){
	System.out.println(" Metodo  :  ListaUser ");
	 List<UserPojo>  lista=Utils.Lista();
	 return lista ;
}

}
