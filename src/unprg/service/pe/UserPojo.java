package unprg.service.pe;

public class UserPojo {
private String userid;
private String username;
private String password;
private String email;
private String ago;
public String getUserid() {
	return userid;
}
public void setUserid(String userid) {
	this.userid = userid;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getAgo() {
	return ago;
}
public void setAgo(String ago) {
	this.ago = ago;
}
public UserPojo( String username, String password, String email,
		String ago) {
	
	
	this.username = username;
	this.password = password;
	this.email = email;
	this.ago = ago;
}

public UserPojo() {

	
}

	
}
