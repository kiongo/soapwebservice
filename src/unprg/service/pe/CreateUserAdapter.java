package unprg.service.pe;

import java.util.List;

import com.google.appengine.api.datastore.EntityNotFoundException;

import unprg.service.pe.jaxws.CreateUser;
import unprg.service.pe.jaxws.CreateUserResponse;
import unprg.service.pe.jaxws.DeleteUser;
import unprg.service.pe.jaxws.DeleteUserResponse;
import unprg.service.pe.jaxws.GetUser;
import unprg.service.pe.jaxws.GetUserResponse;
import unprg.service.pe.jaxws.ListaUserResponse;

public class CreateUserAdapter {

	 WebServiceDemo ws = new WebServiceDemo();
	 
	 public CreateUserResponse createUser(CreateUser request){
		 
		 String userid=request.getArg0();
		 String username=request.getArg1();
		 String password=request.getArg2();
		 String email=request.getArg3();
		 String ago=request.getArg4();
		 
		 boolean b=ws.createUser(userid, username, password, email, ago);
		 
		 CreateUserResponse resp= new CreateUserResponse();
		 resp.setReturn(b);
		 
		 return resp;
	 }
	 
	 public GetUserResponse getUser(GetUser getuser) {
		 
		 String king =getuser.getArg0().toString();
		 System.out.println(" Metodo  :  deleteUser "+king);
		 UserPojo up=null;
		try {
			up = ws.getUser(king);
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			up.setUsername("");
			up.setAgo("");
			up.setEmail("");
			up.setPassword("");
		}
		 GetUserResponse response =new GetUserResponse();
		 
		 response.setReturn(up);
		 
		 return response;
		 
	 }
	 public DeleteUserResponse deleteUser(DeleteUser deleteuser){
		 String userid=deleteuser.getArg0();
		 System.out.println(" Metodo  :  deleteUser "+userid);
		 boolean b=ws.deleteUser(userid);
		 
		 DeleteUserResponse resp= new DeleteUserResponse();
		 resp.setReturn(b);
		 return resp;
	 }
	 public ListaUserResponse listaUser(){
		
		 System.out.println(" Metodo  :  Lista ");
		 List<UserPojo> bxx=ws.ListaUser();
			
		 System.out.println(" Metodo  :  Lista "+bxx);
		ListaUserResponse resp= new ListaUserResponse();
		 resp.setReturn(bxx);
		 return resp;
	 }
	 
}
