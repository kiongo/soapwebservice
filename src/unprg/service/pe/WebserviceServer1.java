package unprg.service.pe;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;


public class WebserviceServer1 extends HttpServlet {
	
	static MessageFactory messageFactory;
	static UserSoapHandler soapHandler;
	
	static {
			try {
				messageFactory=MessageFactory.newInstance();
				soapHandler=new UserSoapHandler();
			}catch (Exception ex) {
				throw new RuntimeException(ex);
			}
	}
	 @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			MimeHeaders headers =getHeaders(req);
			InputStream is =req.getInputStream();
			SOAPMessage soapRequest =messageFactory.createMessage(headers,is);
			SOAPMessage soapResponse=soapHandler.handleSoapMessage(soapRequest);
			
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.setContentType("text/xml;charset=\"UTF-8\"");
			ServletOutputStream os =resp.getOutputStream();
			soapResponse.writeTo(os);
		} catch (SOAPException e) {
			
			throw new IOException("Exception while ");
		}
		
		
	}
	static MimeHeaders getHeaders(HttpServletRequest req) {
		
		Enumeration headerNames =req.getHeaderNames();
		MimeHeaders  headers =new MimeHeaders();
		while (headerNames.hasMoreElements()){
			String headerName = (String) headerNames.nextElement();
			String headerValue =req.getHeader(headerName);
			StringTokenizer values=new StringTokenizer(headerValue,",");
			while(values.hasMoreTokens()){
				headers.addHeader(headerName, values.nextToken().trim());
			}
		}
		return headers;
		
	}

}
